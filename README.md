This repository contains my notes and code from the book [Learning Python Design Patterns](https://www.packtpub.com/application-development/learning-python-design-patterns).  
* My notes are in learning_python_design_patterns.md
* Code samples are in folders organized by chapter and design pattern