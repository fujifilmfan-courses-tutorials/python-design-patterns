#!/usr/bin/python

import module_singleton

print module_singleton.only_one_var
module_singleton.only_one_var += " after modification"

# When module2 is imported, it's code is executed, causing "I'm only one var
# after modification" to be printed to the console.

import module2
