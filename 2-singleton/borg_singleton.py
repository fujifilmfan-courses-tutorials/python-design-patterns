#!/usr/bin/python


class Borg(object):
    _shared_state = {}

    def __new__(cls, *args, **kwargs):

        obj = super(Borg, cls).__new__(cls)
        # deliberately assign the class variable _shared_state to all of the
        # created instances
        obj.__dict__ = cls._shared_state
        return obj

    only_one_var = "I'm only one var"


class Child(Borg):
    pass


class AnotherChild(Borg):
    # reset _shared_state so instances of this class have a different state
    # than other descendants of the Borg class
    _shared_state = {}


"""
>>> from borg_singleton import Borg, Child, AnotherChild
>>> borg = Borg()
>>> another_borg = Borg()
>>> borg is another_borg
False
>>> child = Child()
>>> borg.only_one_var
"I'm only one var"
>>> another_borg.only_one_var
"I'm only one var"
>>> child.only_one_var
"I'm only one var"
>>> borg.only_one_var = "I'm a different var"
>>> borg.only_one_var
"I'm a different var"
>>> another_borg.only_one_var
"I'm a different var"
>>> child.only_one_var
"I'm a different var"
>>> another_child = AnotherChild()
>>> another_child.only_one_var
"I'm only one var"
# another_child does not get the new var, but I don't get the attribute error
# that the book says I'll get
>>> borg.only_one_var = "I'm a third var"
>>> borg.only_one_var
"I'm a third var"
>>> another_borg.only_one_var
"I'm a third var"
>>> child.only_one_var
"I'm a third var"
>>> another_child.only_one_var
"I'm only one var"
"""